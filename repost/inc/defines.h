
#ifndef DEF_DEF_H
#define DEF_DEF_H

typedef struct vehicle_info {
    /**
     * @brief lat
     */
    double x;
    /**
     * @brief longitude
     */
    double y;
    double speed;
    double heading;
    double ax;
    double ay;
}vehicle_info_t;

#define EGO_TO_IP "127.0.0.1"
#define REMOTE_TO_IP EGO_TO_IP
#define EGO_TO_PORT 1208
#define REMOTE_TO_PORT 1209
#define EGO_BIND_PORT 8848
#define REMOTE_BIND_PORT 8849
#endif