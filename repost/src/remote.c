#include <stdio.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <pthread.h>
#include <remote.h>
#include <MessageFrame.h>

char remotemf[1024];
int mf_len;
pthread_mutex_t remote_mf_lock;
int send_trigger = 0;
static e_choice message_choice;
static int
xer2buf(const void *buffer, size_t size, void *app_key)
{
    pthread_mutex_lock(&remote_mf_lock);
    memset(remotemf, 0, size);
    memcpy(remotemf, buffer, size);
    mf_len = size;
    pthread_mutex_unlock(&remote_mf_lock);
    return 0;
}

/**
 * @brief 接受虚拟仿真的自车信息的服务端线程
 */
void virtual_info_recv_thread()
{
    int socketFd;
    struct sockaddr_in servAddr;
    socketFd = socket(AF_INET, SOCK_DGRAM, 0);
    if (socketFd == -1)
    {
        printf("[ERROR] remote server socket created failure!\n");
        exit(-1);
    }
    memset(&servAddr, 0, sizeof(servAddr));
    servAddr.sin_family = AF_INET;
    servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servAddr.sin_port = htons(REMOTE_BIND_PORT);
    if (-1 == (bind(socketFd, (struct sockaddr *)&servAddr, sizeof(servAddr))))
    {
        printf("[ERROR] bind remote server failue!\n");
        exit(-1);
    }
    char recvBuf[1048] = {0};
    BasicSafetyMessage_t *bsm = (BasicSafetyMessage_t *)malloc(sizeof(BasicSafetyMessage_t));
    MessageFrame_t *mf = (MessageFrame_t *)malloc(sizeof(MessageFrame_t));
    while (1)
    {
        memset(recvBuf, 0, sizeof(recvBuf));
        int cnt = recv(socketFd, recvBuf, sizeof(recvBuf), 0);
        if (cnt < 0)
        {
            printf("[ERROR] remote info recv error!\n");
        }
        else
        {

            vehicle_info_t *vi = (vehicle_info_t *)recvBuf;
            bsm->pos.lat = vi->x * 1e7;
            bsm->pos.Long = vi->y * 1e7;
            bsm->speed = vi->speed / 0.02;
            bsm->heading = vi->heading / 0.0125;
            bsm->accelSet.lat = vi->ax / 0.02;
            bsm->accelSet.Long = vi->ay / 0.02;
            mf->present = MessageFrame_PR_bsmFrame;
            mf->choice.bsmFrame = *(bsm);
            if(message_choice==e_choice_BSM){
                xer_encode(&asn_DEF_BasicSafetyMessage,bsm,XER_F_BASIC,xer2buf,0);
            }
            else{
                xer_encode(&asn_DEF_MessageFrame, mf, XER_F_BASIC, xer2buf, 0);
            }
            send_trigger = 1;
        }
    }
}

/**
 * @brief 向协议栈中的sil接口发送数据的客户端线程
 */
void message_transport_send_thread()
{
    int socketFd;
    struct sockaddr_in remote_server_addr;
    bzero(&remote_server_addr, sizeof(remote_server_addr));
    remote_server_addr.sin_family = AF_INET;
    remote_server_addr.sin_addr.s_addr = inet_addr(REMOTE_TO_IP);
    remote_server_addr.sin_port = htons(REMOTE_TO_PORT);
    socketFd = socket(AF_INET, SOCK_DGRAM, 0);
    if (socketFd < 0)
    {
        printf("[ERROR] remote info send socket create failure!\n");
        exit(-1);
    }
    
    while (1)
    {
        if (send_trigger == 0)
            continue;
        usleep(100 * 1000);
        pthread_mutex_lock(&remote_mf_lock);
        sendto(socketFd, remotemf, mf_len, 0,(struct sockaddr*)&remote_server_addr,sizeof(remote_server_addr));
        pthread_mutex_unlock(&remote_mf_lock);
    }
}

void remoteTransport_init(e_choice choice)
{
    message_choice=choice;
    pthread_t recv, send;
    pthread_create(&recv, NULL, virtual_info_recv_thread, NULL);
    pthread_create(&send, NULL, message_transport_send_thread, NULL);
}
